package com.owndevs.neele.neele_possiblemobilechallenge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomListAdapter extends BaseAdapter{
    String [] titles;
    String [] authors;
    String[] imageUrl;

    Context context;
    private static final String TAG_TITLE = "title";
    private static final String TAG_AUTHOR = "author";
    private static final String TAG_IMAGEURL = "imageURL";

    private static LayoutInflater inflater=null;


    public CustomListAdapter(Context _context,ArrayList<HashMap<String, String>> lists) {

        // TODO Auto-generated constructor stub
        titles = new String[lists.size()];
        authors = new String[lists.size()];
        imageUrl = new String[lists.size()];

        for (int i = 0; i < 1000; i++) {
            titles[i] = lists.get(i).get(TAG_TITLE);
            authors[i] = lists.get(i).get(TAG_AUTHOR);
            imageUrl[i] = lists.get(i).get(TAG_IMAGEURL);


        }
        context=_context;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return titles.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv;
        TextView tv2;
        ImageView img;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageLoader imageLoader = ImageLoader.getInstance();

        // Create configuration for ImageLoader (all options are optional)
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .build();
        imageLoader.init(config);
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.listlayout, null);

        holder.tv=(TextView) rowView.findViewById(R.id.textView1);
        holder.tv.setText(titles[position]);
        holder.img=(ImageView) rowView.findViewById(R.id.imageView1);
        imageLoader.displayImage(imageUrl[position], holder.img);
        holder.tv2=(TextView) rowView.findViewById(R.id.textView2);
        holder.tv2.setText(authors[position]);
        rowView.setClickable(false);
        return rowView;
    }
}